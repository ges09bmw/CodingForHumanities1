# Coding for Digital Humanities
My name is Lukas. I am currently enrolled in the Digital Humanities Bachelor Course. I already studied two semesters of Linguistics here in Leipzig and chose to switch my major to Digital Humanities during the seminar „Kreativität und Technik / Wissen in der modernen Gesellschaft“ last semester, which explored the possiblities of digital methods in humanities from a technical aswell as a philosophical point of view and has drawn my interest do Digital Humanities as a whole. 

#### Student Expectations
From the Coding for Digital Humanities Seminar, and the lecture aswell, I expect:
- a deeper understanding concerning the technical background of the digital methods applied
- an overview of the possible fields of application of Digital Humanities
- working on a project to apply the knowledge gained


[This](http://www.reactiongifs.com/wp-content/uploads/2013/10/woah.gif) .gif sums it up pretty accurately.